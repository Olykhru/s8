document.addEventListener('DOMContentLoaded', function()
{
  var trois = new Date("Mon Apr 29 2019 3:00:00 UTC+2"),
    quatre = new Date("Mon May 06 2019 3:00:00 UTC+2"),
    cinq = new Date("Mon May 13 2019 3:00:00 UTC+2"),
    six = new Date("Mon May 20 2019 3:00:00 UTC+2");

  function oui3()
  {
    if((new Date()).getTime() < trois.getTime())
    {
      let time = trois.getTime() - (new Date()).getTime();
      let j = Math.floor(time / (1000 * 60 * 60 * 24));
      time -= j * 1000 * 60 * 60 * 24;
      let h = Math.floor(time / (1000 * 60 * 60));
      time -= h * 1000 * 60 * 60;
      let m = Math.floor(time / (1000 * 60));
      time -= m * 1000 * 60;
      let s = Math.floor(time / (1000));
      if(s < 10)
      {
        s = '0' + s;
      }

      $('.oui')[2].innerText = "Plus que " + (j < 1 ? "" : (j == 1 ? j + " jour " : j  + " jours ")) + (h < 1 ? "" : (h == 1 ? h + " heure " : h  + " heures ")) + (m < 1 ? "" : (m == 1 ? m + " minute " : m + " minutes ")) + (j == 0 && h == 0 && m == 0 ? "" : "et ") + s + " seconde" + (s <= 1 ? "" : "s");
      
      setTimeout(function()
      {
        oui3();
      }, 200);
    }
    else
    {
      $('.oui')[2].innerText = "Yes."
    }
  }
  function oui4()
  {
    if((new Date()).getTime() < quatre.getTime())
    {
      let time = quatre.getTime() - (new Date()).getTime();
      let j = Math.floor(time / (1000 * 60 * 60 * 24));
      time -= j * 1000 * 60 * 60 * 24;
      let h = Math.floor(time / (1000 * 60 * 60));
      time -= h * 1000 * 60 * 60;
      let m = Math.floor(time / (1000 * 60));
      time -= m * 1000 * 60;
      let s = Math.floor(time / (1000));
      if(s < 10)
      {
        s = '0' + s;
      }

      $('.oui')[3].innerText = "Plus que " + (j < 1 ? "" : (j == 1 ? j + " jour " : j  + " jours ")) + (h < 1 ? "" : (h == 1 ? h + " heure " : h  + " heures ")) + (m < 1 ? "" : (m == 1 ? m + " minute " : m + " minutes ")) + (j == 0 && h == 0 && m == 0 ? "" : "et ") + s + " seconde" + (s <= 1 ? "" : "s");

      setTimeout(function()
      {
        oui4();
      }, 200);
    }
    else
    {
      $('.oui')[3].innerText = "Yes."
    }
  }
  function oui5()
  {
    if((new Date()).getTime() < cinq.getTime())
    {
      let time = cinq.getTime() - (new Date()).getTime();
      let j = Math.floor(time / (1000 * 60 * 60 * 24));
      time -= j * 1000 * 60 * 60 * 24;
      let h = Math.floor(time / (1000 * 60 * 60));
      time -= h * 1000 * 60 * 60;
      let m = Math.floor(time / (1000 * 60));
      time -= m * 1000 * 60;
      let s = Math.floor(time / (1000));
      if(s < 10)
      {
        s = '0' + s;
      }

      $('.oui')[4].innerText = "Plus que " + (j < 1 ? "" : (j == 1 ? j + " jour " : j  + " jours ")) + (h < 1 ? "" : (h == 1 ? h + " heure " : h  + " heures ")) + (m < 1 ? "" : (m == 1 ? m + " minute " : m + " minutes ")) + (j == 0 && h == 0 && m == 0 ? "" : "et ") + s + " seconde" + (s <= 1 ? "" : "s");

      setTimeout(function()
      {
        oui5();
      }, 200);
    }
    else
    {
      $('.oui')[4].innerText = "Yes."
    }
  }
  function oui6()
  {
    if((new Date()).getTime() < six.getTime())
    {
      let time = six.getTime() - (new Date()).getTime();
      let j = Math.floor(time / (1000 * 60 * 60 * 24));
      time -= j * 1000 * 60 * 60 * 24;
      let h = Math.floor(time / (1000 * 60 * 60));
      time -= h * 1000 * 60 * 60;
      let m = Math.floor(time / (1000 * 60));
      time -= m * 1000 * 60;
      let s = Math.floor(time / (1000));
      if(s < 10)
      {
        s = '0' + s;
      }

      $('.oui')[5].innerText = "Plus que " + (j < 1 ? "" : (j == 1 ? j + " jour " : j  + " jours ")) + (h < 1 ? "" : (h == 1 ? h + " heure " : h  + " heures ")) + (m < 1 ? "" : (m == 1 ? m + " minute " : m + " minutes ")) + (j == 0 && h == 0 && m == 0 ? "" : "et ") + s + " seconde" + (s <= 1 ? "" : "s");

      setTimeout(function()
      {
        oui6();
      }, 200);
    }
    else
    {
      $('.oui')[5].innerText = "Yes."
    }
  }

  oui3();
  oui4();
  oui5();
  oui6();
});
